<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 08.01.19
 * Time: 17:00
 */

namespace loandbeholdru\shorts;


class classes
{
    public static function akin($obj, $sample, bool $strict = false)
    {
        $type = is_string($sample) && class_exists($sample, true) ?
            $sample : null;
        $type = $type ?? (is_object($sample) ? get_class($sample) : $simple = gettype($sample));

        $class = is_object($obj) ?
            get_class($obj) : $obj;

        if (!class_exists($class, true)) return false;

        $classes = $strict ? [$class] : array_merge(
            [$class], class_parents($class), class_implements($class)
        );

        return ($simple ?? null) ? $type == gettype($obj) : in_array($type, $classes);
    }

    public static function errors(\Throwable $th, string $msg = null)
    {
        $msg = implode(
            PHP_EOL, array_filter([$msg, $th->getMessage()])
        ) .  PHP_EOL;
        $prev = $th->getPrevious();
        $msg .= empty($prev) ? '':
             sprintf(
                "\t%s\n\t%s : %s\n",
                $prev->getMessage(),
                $prev->getFile(),
                $prev->getLine()
            );

        return $msg;
    }

    public static function is_anonymous($classOrObj)
    {
        $class = is_string($classOrObj) ?
            $classOrObj : get_class($classOrObj);

        $class = mb_strtolower($class);
        return !empty(preg_grep('/.*anonymous.*/', [$class]));
    }

    public static function method_exist($obj, string ...$methods)
    {
        foreach ($methods as $method)
            if (method_exists($obj, $method))
                return $method;

        return null;
    }
}