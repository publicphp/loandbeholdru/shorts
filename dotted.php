<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 03.06.18
 * Time: 15:03
 */

namespace loandbeholdru\shorts;


/**
 * Класс позволяет получать данные из массива
 * в dot-адресации.
 * Позволяет производить поиск по dot-ключам regexp-ом
 *
 * Class dotted
 * @package loandbeholdru\shorts
 */
class dotted
{

    protected $payload;
    protected $regexp;
    protected $normalizer;
    protected $reference = [];
    /**
     * dotted constructor.
     * @param $payload
     */
    public function __construct(array $payload, string $regexp = "/\/(.*)\/i?/i", string $normalizer = "/%s/i")
    {
        $this->payload = $payload;
        $this->regexp = $regexp;
        $this->normalizer = $normalizer;
        $this->build($this->payload);
        array_unique($this->reference);
    }

    protected function build(array $current, string $prefix = null)
    {
        foreach ($current as $key => $value) {
            $prefixkey = is_null($prefix) ? $key : "$prefix.$key";
            array_push($this->reference, $prefixkey);
            if (is_array($value))
                $this->build($value, $prefixkey);

       }
    }

    public function get(string $name = null, $default = null)
    {
        if (empty($name))
            return $this->payload;
        $list = explode('.', $name);
        $payload = $this->payload;
        $def = uniqid('stop__');
        foreach ($list as $key) {
            $payload = $payload[$key] ?? $def;
            if ($payload == $def)
                return is_callable($default) ?
                    $default($this) : $default;
        }
        return $payload;
    }

    public function has(string $name)
    {
        $default = uniqid('has_stop__');
        return $default != $this->get($name, $default);
    }

    public function find(string $pattern, $default = null)
    {
        $pattern = preg_replace($this->regexp, "$1", $pattern);
        $pattern = sprintf($this->normalizer, $pattern);
        $out = preg_grep($pattern, $this->references()) ?? [];
        foreach ($out as $key)
            $result[$key] = $this->get($key, $default);
        return $result ?? $default;
    }

    public function findOne(string $pattern, $default = null)
    {
        $result = $this->find($pattern, $default);
        return is_array($result) ?
            array_values($result)[0] : $result;
    }
    public function findOneWithKey(string $pattern, $default = null)
    {
        $result = $this->find($pattern, $default);
        return is_array($result) ?
            array_slice($result,0,1,true) : $result;
    }

    public function list()
    {
        foreach ($this->reference as $key)
            $result[$key] = $this->get($key);
        return $result;
    }

    public function references()
    {
        return $this->reference;
    }

    public static function create(array $payload)
    {
        return new self($payload);
    }
    public static function fromArray(array $payload)
    {
        return new self($payload);
    }

//    public static function fromList(array $list)
//    {
//        $payload = [];
//        foreach ($list as $key => $value) {
//            $path = array_reverse(explode('.', $key));
//            $last = count($path) - 1;
//            $branch = $value;
//            foreach ($path as $index => $step) {
//                $tmp[$step] = $branch;
//            }
//            $payload = array_merge_recursive($payload, $branch);
//        }
//        return self::fromArray($payload);
//    }

}