<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 01.06.18
 * Time: 14:39
 */

namespace loandbeholdru\shorts;


/**
 * Предоставляет методы для реализации возможности преобразования массива в класс и наоборот
 * Предполагает, что список преобразуемых полей находится в массиве-константе:
 * 'PACK' либо 'OPTS' либо 'OPTIONS'
 *
 * Данные хранит либо в свойствах либо в массиве payload
 * Извлекает данные сначала из свойств, затем при помощи методов $название_свойства() затем из payload
 *
 * Для замены пустого значение может испоьзовать содержимое константы EMPTY
 * 
 * При экспорте добавляет в массив arrays::TYPE_KEY и записывает туда название класса
 * При импорте ищет в массиве arrays::TYPE_KEY и создает класс с названием от туда
 * 
 * Trait fromToArrayWithClassTrait
 * @package loandbeholdru\shorts
 */
trait fromToArrayWithClassTrait
{
    public function toArray(array $filter = [])
    {
        $_type = static::class;
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS') ?? [];
        $fields = array_diff($fields, $filter);
        foreach ($fields as $field)
            $result[$field] = $this->$field ?? $this->$field() ??
                $this->payload[$field] ?? static::ifDefined('EMPTY');

        $const = static::ifDefined('PAYLOAD', 'unknownfield');
        $payload = $result[$const] ?? [];
        foreach ($payload as $name => $val)
            if (is_object($val))
                $result[$const][$name] = method_exists($val, 'toArray') ?
                    $val->toArray() : $val;

        return compact(arrays::TYPE_KEY) + arrays::deeparray($result ?? []);
    }

    public static function fromArray(array $data)
    {
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);
        foreach ($fields as $field)
            $opts[] = // arrays::fromClassified($data[$field]) ??
                $data[$field] ?? static::ifDefined('EMPTY');
        $type = $data[arrays::TYPE_KEY] ?? static::class;
        $exception = self::ifDefined('EXCEPTION', \Exception::class);

        if (empty($opts) && empty($type))
            throw new $exception(
                '"_type" not found in $data-array and "self::PACK" - not found!'
            );
        foreach ($opts as &$opt)
            $opt = arrays::fromClassified($opt, $opt);

        return empty($type) || $type === static::class ?
            new static(...$opts) : $type::fromArray($data);
    }
    public function isEmpty()
    {
        return empty(array_filter($this->toArray()));
    }

    public static function ifDefined(string $constname, $default = null)
    {
        return defined(static::class . '::' . $constname) ?
            constant(static::class . '::' . $constname) :
            arrays::fail($constname, $default);
    }
}