<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 29.01.19
 * Time: 15:18
 */

namespace loandbeholdru\shorts;


trait initTrait
{
    protected function init(array $data, array $pack = null)
    {
        $pack = $pack ?? arrays::ifDefined(
            static::class, 'PACK', array_keys(get_object_vars($this))
        );
        foreach ($pack as $var)
            $this->$var = $data[$var] ?? $this->$var;

        return $this;
    }
}