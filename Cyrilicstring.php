<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 22.04.17
 * Time: 10:30
 */

namespace loandbeholdru\shorts;


trait Cyrilicstring
{
    private $delimiters = ' .,!-:;/\\';
    /**
     * Подсушивает строку и приводит строку к виду "Строка"
     * Обрабатывает кирилицу
     * @param string $str
     * @param bool $drying = true
     * @param string $encode = "UTF-8"
     * @return mixed|string
     */
    public function firstCAP(string $str, bool $drying = true, string $encode = "UTF-8"){
        $str = mb_strtolower(
            ($drying ? trim($str,$this->delimiters) : $str));
        return
            mb_strtoupper(mb_substr($str, 0, 1, $encode), $encode) .
            mb_substr($str, 1, mb_strlen($str, $encode), $encode);
    }

    /**
     * Поиск по массиву на нестрогое совпадение (вхождение) $lookfor в элементы массива
     * Возвращает первое совпадение
     * Поиск регистронезависимый. Поддерживает кирилицу.
     * @param string|array $list
     * @param string $lookfor
     * @return bool|mixed
     */
    public function instr($list, string $lookfor){
        $stanza = trim($lookfor,$this->delimiters);
        if (is_string($list)) $list = [$list];
        foreach ($list as $str) {
            if (preg_match("/" . $stanza . "/ui", $str)) return $str;
        }
        return false;
    }
    /**
     * Поиск по ключам ассоциативного массива на нестрогое совпадение (вхождение) $lookfor в ключах массива
     * Возвращает первое совпадение.
     * Поиск регистронезависимый. Поддерживает кирилицу.
     * @param array $list
     * @param string $lookfor
     * @return bool|mixed
     */
    public function instrkey(array $list, string $lookfor){
        $stanza = trim($lookfor,$this->delimiters);
        foreach ($list as $str => $value) {
            if (preg_match("/" . $stanza . "/ui", $str)) return $str;
        }
        return false;
    }
}