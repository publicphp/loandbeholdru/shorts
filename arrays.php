<?php
/**
 * Created by PhpStorm.
 * User: loandbeholdru
 * Date: 06.06.18
 * Time: 18:30
 */

namespace loandbeholdru\shorts;


/**
 * Class arrays
 * @package loandbeholdru\shorts
 */
class arrays
{
    
    const TYPE_KEY = '_type';
    const UID = 'uid';
    // REGEXP для выделения полей из $regexp
    const FIELDS_REGEXP = '/.*(\?(P)*\<(\w+)\>).+/U';
    const OPTIONS_REGEXP = '/(.*\/)(?P<keys>[imsxADUu]*)$/';

    /**
     * arrays constructor.
     */
    private function __construct()
    {
    }

    public static function insertBefore(int $key, $value, ...$payload)
    {
        $key = $key < 0 ? 0 : $key;
        $min = min(array_keys($payload));
        $max = max(array_keys($payload));

        $key = $key < $min ? $min : $key ;
        $key = $key > $max ? $max + 1 : $key ;

        $first = array_slice($payload, $min, $key - $min);
        $last = array_diff($payload, $first);

        return array_merge($first, [$value], $last);
    }

    public static function insertAfter(int $key, $value, ...$payload)
    {
        $key = $key < 0 ? -1 : $key;
        $min = min(array_keys($payload));
        $max = max(array_keys($payload));

        $key = $key + 1 < $min ? $min  : $key + 1;
        $key = $key + 1 > $max ? $max + 1  : $key + 1;

        $first = array_slice($payload, $min, $key - $min - 1);
        $last = array_diff($payload, $first);

        return array_merge($first, [$value], $last);
    }
    
    public static function lowkeys(array $payload)
    {
        foreach ($payload as $key => $val)
            $ret[strtolower($key)] = $val;

        return $ret ?? $payload;
    }
    public static function few($payload, string $nodelimetrs = '-_+', $default = null)
    {
        if (empty($payload)) 
            return $payload = static::fail($payload, $default);
        
        $nodelimetrs = preg_quote($nodelimetrs,'+.*/');
        preg_match_all("/([\w$nodelimetrs]+)/", $payload, $out);

        return $payload = end($out);
    }


    public static function always($payload)
    {
        return self::deeparray($payload, true);
    }

    /**
     * Сравнивает два массива, предварительно рекурсивно отсортировав
     *
     * @param array $ar1
     * @param array $ar2
     * @return bool
     */
    public static function isEqual(array $ar1, array $ar2)
    {
        self::sort($ar1);
        self::sort($ar2);
        array_walk($ar1, [self::class, 'sort']);
        array_walk($ar2, [self::class, 'sort']);
        return json_encode($ar1) === json_encode($ar2);
    }

    /**
     * Возвращает true, если все элементы массива принадлежат типу $type
     *
     * @param $payload          - проверяемые данные
     * @param string $sample    - образец типа
     * @param bool $anywayarray - конвертировать ли в массив данные (если они не массив)
     * @return bool
     */
    public static function is_array_of($payload, $sample, bool $anywayarray = false)
    {
        $type = is_string($sample) && class_exists($sample, true) ?
            $sample : null;
        $type = $type ?? (is_object($sample) ? get_class($sample) : gettype($sample));

        $payload = !is_array($payload) && $anywayarray ? [$payload] : $payload;
        if (!is_array($payload)) return false;

        foreach ($payload as $item){
            $test1 = is_object($item);
            $test1 = is_string($item) ? ($test1 || class_exists($item, true)) : $test1; 
            $test1 = $test1 ? classes::akin($item, $type) : gettype($item) == $type;
            if (!$test1) break;
        }

        return $test1 ?? true;
    }
    /**
     * Комбинирует массив из двух разных. Независимо от длины. Добивает все $default
     *
     * @param $names
     * @param $values
     * @param null $default
     * @return array
     */
    public static function args($names, $values, $default = null)
    {
        $names = self::valid_json($names, true, $names);
        $values = self::valid_json($values, true, $values);

        if (!is_array($names) || !is_array($values))
            return self::fail(compact('names', 'values'), $default);

        $names = self::isAssoc($names) ? array_keys($names) : $names;
        $values = array_values($values);

        $values = array_slice($values,0, count($names));

        $test1 = is_array($default) ? array_intersect_key($default, array_flip($names)) : null;
        $test1 = empty($test1) ? null : $test1;

        $placeholder = empty($test1) ? $default : uniqid();

        $values = count($names) != count($values) ?
            array_pad($values, count($names), $placeholder) : $values;

        $result = array_combine($names, $values);

        if (!empty($test1))
            foreach ($result as $key => &$item)
                $item = $item == $placeholder ?
                    ($test1[$key] ?? null ) : $item;

        return $result;
    }

    public static function anyway($data, string $left = null, string $right = null, string $del = ':')
    {
        $data = empty($left) ? $data : self::lanyway($data, $left, $del);
        return empty($right) ? $data : self::ranyway($data, $right, $del);
    }

    public static function ranyway($data, string $char, string $del = ':')
    {
        $data = static::deepimplode($del, $data);
        return empty($data) ? $data :
            rtrim($data, $char) . $char;
    }

    public static function lanyway($data, string $char, string $del = ':')
    {
        $data = static::deepimplode($del, $data);
        return empty($data) ? $data :
            $char . ltrim($data, $char);
    }

    /**
     * Рекурсивно сортирует массив по ключам либо по содержимому (если он обыкновенный)
     *
     * @param $payload
     * @return bool
     */
    public static function sort(&$payload)
    {
        $payload = self::valid_json($payload, true, $payload);
        if (!is_array($payload))
            return true;

        array_walk($payload, [self::class, 'sort']);

        if (self::isAssoc($payload))
            ksort($payload);
        else
            sort($payload);

    }

    /**
     * Определяет ассоциативность массива
     *
     * @param $payload
     * @return bool
     */
    public static function isAssoc($payload)
    {
        $payload = self::valid_json($payload) ?? $payload;
        if (!is_array($payload)) return false;
        $tools = new class{use havetobeOperations;};
        return $tools->is_assoc($payload);
    }

    /**
     * Возвращает экземпляр класса сделанный из массива,
     * если массив содержит информацию о классе в поле с названием:
     *  arrays::TYPE_KEY
     *
     * @param $classified   -   массив либо json
     * @param null $default -   индикатор либо обработчик ошибки
     * @return mixed
     */
    public static function fromClassified($classified, $default = null)
    {
        $classified = self::valid_json($classified, true) ?? $classified;

        $class = is_array($classified) ?
            ($classified[self::TYPE_KEY] ?? null) : null;

        return is_null($class) ?
            self::fail($classified, $default) : $class::fromArray($classified);
    }

    public static function fail($data, $default)
    {
        if ($default instanceof \Exception) throw $default;
        return is_callable($default) && !in_array(strtolower($default), get_defined_functions()['internal']) ?
            $default($data) : $default;
    }

    /**
     * Возвращает декодированный JSON, действуя так же как и json_decode,
     * только определяет валидность json. В случае не валидного JSON возвращает
     * либо default либо результат выполнения default
     *
     * @param $string
     * @param bool $assoc
     * @param null $default
     * @return mixed
     */
    public static function valid_json($string, bool $assoc = true, $default = null)
    {
        if (!is_string($string)) return self::fail($string, $default);
        $result = json_decode($string, $assoc);
        return (json_last_error() == JSON_ERROR_NONE) ? $result : self::fail($string, $default);
    }

    public static function preg_grep(string $regexp, $data, bool $oldkeys = false)
    {
        $data = is_array($data) || is_object($data) ?
            json_decode(json_encode($data), true) : [$data];
        $out = [];
        array_walk_recursive($data,function ($val, $key)use($regexp, &$out){
            $out[$key] = is_array($val) ?
                arrays::preg_grep($regexp, $val) : preg_grep($regexp, [$val]);
            $out = array_filter($out);
        });
        return $oldkeys ? array_filter($out) :
            array_values(array_filter($out));
    }

    public static function findByKey(string $lookfor, $work, $default = [])
    {
        preg_match(self::OPTIONS_REGEXP, $lookfor, $keys);
        $lookfor = empty($keys) ? self::anyway($lookfor,'/', '/') : $lookfor;

        $work = is_array($work) ? $work : [];
        foreach ($work as $key => $value) {
            if (preg_match($lookfor, $key))
                return $value;
            $value = is_object($value) ?
                (method_exists($value, 'toArray') ? $value->toArray() : json_decode(json_encode($value), true)) :
                $value;
            $value = is_array($value) ? static::findByKey($lookfor, $value) : [];
            if (!empty(array_filter(is_array($value) ? $value : [$value])))
                return $value;
        }
        return arrays::fail($work, $default);
    }

    public static function findAllByKey(
        string $lookfor, $work, array $result = [], string $dottedkey = '', callable $resulter = null
    )
    {
        preg_match(self::OPTIONS_REGEXP, $lookfor, $keys);
        $lookfor = empty($keys) ? self::anyway($lookfor,'/', '/') : $lookfor;

        $work = is_array($work) ? $work : [];
        foreach ($work as $key => $value) {
            $smartkey = '' === $dottedkey ? $key : "$dottedkey.$key";
            if (preg_match($lookfor, $key))
                $result[$smartkey] = $value;
            $value = is_object($value) ?(
                method_exists($value, 'toArray') ?
                    $value->toArray() : json_decode(json_encode($value), true)
            ) : $value;
            $value = static::findAllByKey($lookfor, $value, $result, $smartkey, $resulter);
            $value = is_callable($resulter) ? $resulter($value) : $value;
            $result = array_merge($result, $value);
        }
        return $result ?? [];
    }

    public static function findAllByVal(
        string $lookfor, $work, array $result = [], string $dottedkey = '', callable $resulter = null
    ){
        preg_match(self::OPTIONS_REGEXP, $lookfor, $keys);
        $lookfor = empty($keys) ? self::anyway($lookfor,'/', '/') : $lookfor;

        $work = is_array($work) ? $work : [];
        foreach ($work as $key => $value) {
            $smartkey = '' === $dottedkey ? $key : "$dottedkey.$key";
            try{
                if (is_object($value) || is_array($value))
                    throw new \Exception();
                if (preg_match($lookfor, $value))
                    $result[$smartkey] = $value;
            }catch (\Throwable $e){
                $value = is_object($value) ?(
                method_exists($value, 'toArray') ?
                    $value->toArray() : json_decode(json_encode($value), true)
                ) : $value;

            }
            $value = static::findAllByVal($lookfor, $value, $result ?? [], $smartkey, $resulter);
            $value = is_callable($resulter) ? $resulter($value) : $value;
            $result = array_merge($result, $value);
        }
        return $result ?? [];
    }
    // Преобразует многомерный массив в одномерный массив dot-нотации
    public static function array2dot($arr, $narr = array(), $nkey = '') {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $narr = array_merge($narr, static::array2dot(
                    $value, $narr, $nkey . $key . '.'));
            } else {
                $narr[$nkey . $key] = $value;
            }
        }

        return $narr;
    }
    // Получает значение из массива по ключу, записанному в dot-нотации
    public static function getByDotted(string $name, $payload, $default = null)
    {
        $payload = is_array($payload) ? $payload : [];
        $list = explode('.', $name);
        $def = uniqid('stop__');
        foreach ($list as $key) {
            $payload = $payload[$key] ?? $def;
            if ($payload == $def)
                return is_callable($default) ?
                    $default($name, $payload) : arrays::fail($name, $default);
        }
        return $payload;
    }

    /**
     * Сначала привод все к массиву (по всей глубине),
     * потом к json, а потом берет hash.
     *
     * @param $payload
     * @param string $algo
     * @return string
     */
    public static function deephash($payload, string $algo = 'ripemd128')
    {
        $payload = self::valid_json($payload, true) ??  $payload;
        $payload = is_array($payload) ? $payload : [$payload];
        array_walk_recursive($payload, function (&$value, $key){
            $value = is_object($value) ?
                (method_exists($value, '__toString') ? (string)$value :
                    (method_exists($value, 'toArray') ?
                        json_encode($value->toArray()) : json_encode($value))) : $value;
        });
        return hash($algo, json_encode($payload));
    }

    /**
     * Сначала приводит все к массиву (по всей глубине),
     * потом к форматам для ассоцивтивного и для обычного массива.
     *
     * @param $payload
     * @param string $algo
     * @return string[]
     */
    public static function deepstring($payload, string $assoc = "%s : %s", string $ordinary = "%s", string $del = ', ')
    {
        $payload = self::valid_json($payload, true) ??  $payload;
        try{
            $payload = is_array($payload) ? $payload : [(string)$payload];
        }catch (\Throwable $e){
            $payload = is_array($payload) ? $payload : [$payload];
        }

        $result = [];
        array_walk($payload, function (&$value, $key)use($assoc, $ordinary, &$result, $del){
            $value = is_object($value) ?
                (method_exists($value, '__toString') ? (string)$value :
                    (method_exists($value, 'toArray') ?
                        json_encode($value->toArray()) : json_decode(json_encode($value), true))) : $value;
            
            $value = is_string($value) ? [$value] : $value;
            $value = static::is_array_of($value, 'string') ? 
                $value : static::deepstring($value, $assoc, $ordinary, $del);
            $value = implode($del, $value);

            if (!is_numeric($key))
                $value = sprintf($assoc, $key, $value);

        });


        return $payload;
    }

    public static  function deepimplode(string $del, $payload, string $assocformat = "%s : %s")
    {
        $payload = static::deepstring($payload);
        if (self::isAssoc($payload))
            foreach ($payload as $key => $value) 
                $res[] = sprintf($assocformat, $key, $value);
            
        return implode($del, $res ?? $payload);
    }

    public static function window(array $payload, $start = null, $finish = null)
    {
        $finish = $finish ?? uniqid();
        return array_filter($payload, function ($val, $key)use(&$start, &$finish){

            $rfinish = $finish;

            $start = $start == $val ? null : $start;
            $finish = $finish == $val ? null : $finish;

            return empty($start) && !empty($rfinish);

        }, ARRAY_FILTER_USE_BOTH);
    }
    public static function kwindow(array $payload, $kstart = null, $kfinish = null)
    {
        $kfinish = $kfinish ?? uniqid();
        return array_filter($payload, function ($val, $key)use(&$kstart, &$kfinish){

            $rfinish = $kfinish;

            $kstart = $kstart == $key ? null : $kstart;
            $kfinish = $kfinish == $key ? null : $kfinish;

            return empty($kstart) && !empty($rfinish);

        }, ARRAY_FILTER_USE_BOTH);
    }

    public static function last(array $payload)
    {
        return end($payload);
    }

    public static function first(array $payload)
    {
        return reset($payload);
    }


    public static function klast(array $payload)
    {
        end($payload);
        return key($payload);
    }

    public static function kfirst(array $payload)
    {
        reset($payload);
        return key($payload);
    }
    /**
     * Приводит все к сотсоянию массива (по всей глубине)
     *
     * @param $payload
     * @return array
     */
    public static function deeparray($payload, bool $anywayarray = false)
    {
        
        $payload = self::valid_json($payload, true) ?? $payload;
        $payload = (!is_array($payload)) && $anywayarray ? [$payload] : $payload;
        $test1 = is_array($payload) || $anywayarray;
        $payload = $test1 ? $payload : [$payload];
        array_walk_recursive($payload, function (&$value, $key){
            $value = objToJson::done($value) ?
                json_decode($value, true) : $value;
            $value = arrays::valid_json($value, true, $value);
        });
        return $test1 ? $payload : array_shift($payload);
    }
    public static function ifDefined(string $classname, string $constname, $default = null)
    {
        $const = implode('::', array_filter([$classname, $constname]));
        return defined($const) ? constant($const) : static::fail(compact('classname', 'constname'), $default);
    }
    // Возвращает значение с ключем $key, убирая его из массива 
    public static function pull($key, &$payload, $default = null)
    {
        $val = $payload[$key] ?? static::fail($payload, $default);
        unset($payload[$key]);
        return $val;
    }
    /**
     * Разбивает строку(и) по полям, обозначенным в $regexp.
     * Применяет функцию $proc к результату.
     * Возвращает массив либо $default
     *
     * @param string $regexp
     * @param callable|null $proc
     * @param array $default
     * @param string ...$lines
     * @return array
     */
    public static function linesByFieldsProc(string $regexp, callable $proc = null, $default = [], string ...$lines)
    {
        $regexp = trim($regexp, '/');

        preg_match_all(static::FIELDS_REGEXP, $regexp, $fields);
        $fields = end($fields);
        if (empty($fields)) return static::fail("/$regexp/", $default);
        $fields = array_flip($fields);
        foreach ($lines as &$line) {
            preg_match("/$regexp/", $line, $out);
            $line = array_intersect_key($out, $fields);
            $line = is_callable($proc) ? $proc($line) : $line;
        }
        return array_filter($lines);
    }

    /**
     * Разбивает строку(и) по полям, обозначенным в $regexp.
     * Возвращает массив.
     *
     * @param string $regexp
     * @param string ...$lines
     * @return array
     */
    public static function linesByFields(string $regexp, string ...$lines)
    {
        return static::linesByFieldsProc($regexp, function ($val){
            return array_filter($val);
        }, [], ...$lines);
    }

    /**
     * Разбивает строку(и) по полям, обозначенным в $regexp.
     * Возвращает первую удачную строку.
     *
     * @param string $regexp
     * @param string ...$lines
     * @return array
     */
    public static function firstByFields(string $regexp, string ...$lines)
    {
        $all = static::linesByFieldsProc($regexp, null, [], ...$lines);
        return array_shift($all);
    }

    /**
     * Если утверждение $failIFtrue - истинно, выбрасывает исключение
     * с сообщением $msg и кодом $code
     *
     * @param bool $failIFtrue
     * @param $msg
     * @param int $code
     * @return string
     * @throws \Exception
     */
    public static function assert(bool $failIFtrue, $msg, int $code = 0)
    {
        if ($failIFtrue)
            if (is_callable($msg)){
                arrays::fail($code);
            }else{
                $e = $msg instanceof \Exception ? $msg : new \Exception($msg, $code);
                throw $e;
            }


       return __CLASS__;
    }

    /**
     * Возвращает выбор данных по ключам во всех $template's из $workpiece
     * Приводит оба параметра к виду ассоциативного массива
     * Аналог array_intersect_key() с предварительными преобразованиями
     *
     * @param $workpiece
     * @param $template
     * @return array
     */
    public static function cutByKeys($workpiece, ...$templates)
    {
        $workpiece = static::deeparray($workpiece, true);
        $workpiece = static::isAssoc($workpiece) ? $workpiece : array_flip($workpiece);
        foreach ($templates as &$template) {
            $template = static::deeparray($template, true);
            $template = static::isAssoc($template) ? $template : array_flip($template);
        }
        
        $template = empty($templates) ? [] : array_merge(...$templates);

        return array_intersect_key($workpiece, $template);
    }

    /**
     * Возвращает выбор данных по ключам не содержащимся ни в одном из
     * $template's из $workpiece. Приводит оба параметра к виду
     * ассоциативного массива.
     * Аналог array_diff_key() с предварительными преобразованиями
     *
     * @param $workpiece
     * @param $template
     * @return array
     */
    public static function filterByKeys($workpiece, ...$templates)
    {

        if (empty($templates)) return $workpiece;

        $workpiece = static::deeparray($workpiece, true);
        $workpiece = static::isAssoc($workpiece) ? $workpiece : array_flip($workpiece);
        foreach ($templates as &$template) {
            $template = static::deeparray($template, true);
            $template = static::isAssoc($template) ? $template : array_flip($template);
        }

        $template = empty($templates) ? [] : array_merge(...$templates);

        return array_diff_key($workpiece, $template);
    }

    /**
     * Упорядочивает $payload по $keys
     * Все что не вошло в keys добавляет в конец
     * Ненайденные добавляет null
     *
     * @param array $payload
     * @param string ...$keys
     * @return array
     */
    public static function order(array $payload, string ...$keys)
    {
        $res = [];
        foreach ($keys as $key) {
            $res[$key] = $payload[$key] ?? null;
            unset($payload[$key]);
        }
        return $res + $payload;
    }


    /**
     * Склеивает содержимое двух массивов по ключам
     *
     * @param array $src1
     * @param array $src2
     * @param null $default
     * @return array
     */
    public static function together(array $src1, array $src2, $default = null)
    {
        foreach ($src1 as $key => $value) {
            $line = $src2[$key] ?? $default;
            $value = is_object($value) ? arrays::always($value) : $value;
            if (is_array($value))
                $ret[$key] = is_array($line) ?
                    arrays::args($value, $line, $default) : [json_encode($value) => $line];
            else
                $ret[$key] = [$value => $line];
        }
        return $ret ?? [];
    }

    /**
     * Заполняет доступные свойства класса данными из массива с одноименными полями
     * 
     * @param object $msg
     * @param array $payload
     * @param string ...$filtred
     * @return object
     */
    public static function enrich(object $msg, array $payload, string ...$filtred)
    {
        $class = get_class($msg);
        $vars = array_diff(get_class_vars($class), $filtred);
        foreach ($vars as $var)
            $msg->$var = $payload[$var] ?? $msg->$var;

        return $msg;
    }
    // Возвращае класс без неймспейса
    public static function class(string $full)
    {
        return preg_replace('/\S+\\\w+$/','$2', $full);
    }

    public static function findFields(array $payload, string ...$fields)
    {
        $regexp = '/(\.\*)|(\.\+)|([\^\$\?\(\)\[\]\{\}\/])/';
        foreach ($fields as $field) {
            $key = preg_replace($regexp, '', $field);
            $ret[$key] = static::findByKey($field, $payload, null);
        }

        return array_filter($ret ?? []);
    }
    public static function renameFields(array $payload, array $fields)
    {
        $regexp = '/(\.\*)|(\.\+)|([\^\$\?\(\)\[\]\{\}\/])/';
        foreach ($fields as $field => $new) {
            $key = preg_replace($regexp, '', $field);
            $ret[$new] = static::findByKey($field, $payload, null);
        }

        return array_filter($ret ?? []);
    }

    // Заменяет поля в тексте $payload по словарю $dict. ReGexp шаблон поля берет из $fieldregexp
    // $dict - ассоциативный массив. Названия поле в $keys, содержимое полей в $values
    public static function substfields(string $payload, array $dict, string $fieldregexp = "/#\{%s\}/")
    {
//        if (!static::isAssoc($dict))
//            throw new \Exception(
//                "You have to take assoc array as dict!");

        foreach ($dict as $key => $val)
            if (is_array($val)){
                $payload = static::substfields($payload, $val , $fieldregexp);
            }else{
                $regexp = sprintf($fieldregexp, $key);
                $payload = preg_replace($regexp, $val, $payload);
            }

        return $payload;
    }
}
