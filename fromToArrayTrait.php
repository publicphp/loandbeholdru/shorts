<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 24.05.18
 * Time: 19:39
 */

namespace loandbeholdru\shorts;


/**
 * Предоставляет методы для реализации возможности преобразования массива в класс и наоборот
 * Предполагает, что список преобразуемых полей находится в массиве-константе:
 * 'PACK' либо 'OPTS' либо 'OPTIONS'
 *
 * Данные хранит либо в свойствах либо в массиве payload
 * Извлекает данные сначала из свойств, затем при помощи методов $название_свойства() затем из payload
 *
 * Для замены пустого значение может испоьзовать содержимое константы EMPTY
 *
 * Trait fromToArrayTrait
 * @package loandbeholdru\shorts
 */
trait fromToArrayTrait
{

    public function toArray(array $filter = [])
    {
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);
        $fields = array_diff($fields, $filter);
        foreach ($fields as $field)
            $result[$field] = $this->$field ?? $this->$field() ??
                $this->payload[$field] ?? static::ifDefined('EMPTY');

        $const = static::ifDefined('PAYLOAD', 'unknownfield');
        $payload = $result[$const] ?? [];
        foreach ($payload as $name => $val)
            if (is_object($val))
                $result[$const][$name] = method_exists($val, 'toArray') ?
                    $val->toArray() : $val;

        return $result;
    }

    public static function fromArray(array $data)
    {
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);
        foreach ($fields as $field)
            $opts[] = $data[$field] ?? static::ifDefined('EMPTY');
        return new static(...$opts);
    }

    public function isEmpty()
    {
        return empty(array_filter($this->toArray()));
    }

    public static function ifDefined(string $constname, $default = null)
    {
        return defined(static::class . '::' . $constname) ?
            constant(static::class . '::' . $constname) :
            arrays::fail($constname, $default);
    }
}