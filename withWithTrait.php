<?php


namespace loandbeholdru\shorts;


trait withWithTrait
{
    public function __call($name, $arguments)
    {
        if (in_array($name, static::PACK))
            return $this->$name ?? (
                property_exists(get_class($this), $name) ?
                    $this->$name : $this->payload[$name]
            );
        $fields = arrays::linesByFields('^with(?<var>\S+)', $name);
        extract(end($fields));
        $var = strtolower($var);
        if (!in_array($var, static::PACK))
            throw new \Exception("'$var' is not configured in " . __CLASS__ );
        $value = array_shift($arguments);
        $classes = arrays::ifDefined(static::class, 'DIFFERED', []);
        $class = $classes[$var] ?? '';
        $allowed = is_object($value) ? explode('|', $class) : [];
        $present = is_object($value) ?
            array_merge(class_parents($value), [get_class($value)]) : [];

        if (!empty($allowed) && empty(array_intersect($allowed, $present)))
            throw new \Exception("Wrong class of value: '$var' => " . get_class($value));

        $args = [$var => $value] + $this->toArray();
        return static::fromArray($args);
    }

}