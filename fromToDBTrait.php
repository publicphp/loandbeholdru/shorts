<?php


namespace loandbeholdru\shorts;

use RedBeanPHP\OODBBean;
use RedBeanPHP\R;

/**
 * Трейт для автоматического преобразования immutable объекта к bean для redbeanPHP
 * Необходимо декларировать константы:
 * DBTRANSFORM - массив клюя-значение, где ключ - поле сущности, значение - поле bean
 * DBTABLE - название таблицы
 * KEYS - перечень полей, которые явялются ключевыми для сущности
 *
 *
 * Trait fromToDBTrait
 * @package loandbeholdru\shorts
 */
trait fromToDBTrait
{

    /**
     * Возвращает название таблицы, в которую записывается сущность
     *
     * @return string
     * @throws \Exception
     */
    public function dbtable()
    {
        return static::ifDefined('DBTABLE') ??
            arrays::fail('DBTABLE', new \Exception(sprintf(
                'You have to declare DBTABLE-constant in %s', static::class
            )));
    }

    /**
     * Возвращает массив полей, которые считаются ключевыми при поиске сущности
     *
     * @param bool $transform
     * @return mixed
     * @throws \Exception
     */
    public function dbkeys(bool $transform = false)
    {
        $fields = static::ifDefined('KEYS') ??
            arrays::fail('KEYS', new \Exception(sprintf(
                'You have to declare KEYS-constant in %s', static::class
            )));


        return $transform ? $this->transform(...$fields) :
            arrays::cutByKeys($this->toArray(), ...$fields);
    }

    /**
     * Создает сущность из bean-объекта
     *
     * @param OODBBean $bean
     * @return mixed
     */
    public static function fromDB(OODBBean $bean)
    {
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);

        $payload = $bean->export();
        $payload[arrays::TYPE_KEY] = static::class;
        $payload = static::reconvert($payload);

        return arrays::fromClassified($payload);
    }

    /**
     * Превращает сущность в bean
     *
     * @return array
     * @throws \Exception
     */
    public function toDB()
    {
        $payload = $this->transform();
        foreach ($payload as $key => &$value) {
            if (!is_array($value)) continue;
            if (!empty($value[arrays::TYPE_KEY])){
                $value = arrays::fromClassified($value);
                continue;
            }
            foreach ($value as &$item)
                $item = arrays::fromClassified($item)->toDB();
        }
        $payload['_type'] = $this->dbtable();
        return $payload;
    }

    /**
     * Возвращает массив сущности с приведенными к bean ключами полей
     *
     * @param mixed ...$keys
     * @return array
     * @throws \Exception
     */
    public function transform(...$keys)
    {
        $transform = static::ifDefined('DBTRANSFORM') ??
            arrays::fail('DBTRANSFORM', new \Exception(sprintf(
                'You have to declare DBTRANSFORM-constant in %s', static::class
            )));

        $data = $this->toArray();

        $transform = $transform + array_combine(static::PACK, static::PACK);
        $transform = empty($keys) ? $transform : arrays::cutByKeys($transform, ...$keys);

        foreach ($transform as $th => $db)
            if (is_array($db))
                $payload[arrays::first($db)][] = $this->convert($data[$th]);
            else
                $payload[$db] = $this->convert($data[$th]);

        return $payload;
    }

    protected function convert($item = null)
    {
        $item = $item ?? static::ifDefined('EMPTY') ?? null;
        return method_exists($item, 'toArray') ? $item->toArray() : $item;
    }

    protected static function reconvert(array $payload)
    {
        $transform = static::ifDefined('DBTRANSFORM') ??
            arrays::fail('DBTRANSFORM', new \Exception(sprintf(
                'You have to declare DBTRANSFORM-constant in %s', static::class
            )));

        $diff = static::ifDefined('DIFFERENT', []);

        foreach ($transform as &$item)
            $item = is_array($item) ? arrays::last($item) : $item;

        $db = array_values($transform);
        $untransform = array_flip($transform);

        foreach ($payload as $key => $val){
            $real = arrays::preg_grep(sprintf("/^(.*%s%s|%s)$/i", $key, 'List', $key), $db);
            if (empty($real)) continue;
            $real = arrays::last(arrays::last($real));
            $key = $untransform[$real];
            $transform[$key] = empty($diff[$key]) ? $val :
                [arrays::TYPE_KEY => $diff[$key]] + arrays::last($val);
        }
        return $transform + $payload;

    }

}