<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 26.01.19
 * Time: 0:49
 */

namespace loandbeholdru\shorts;


trait compactTrait
{
    protected function compact(string ...$names)
    {
        $names = empty($names) ? array_keys(get_object_vars($this)) : $names;
        foreach ($names as $name)
            if (isset($this->$name))
                $ret[$name] = $this->$name;

        return $ret ?? [];
    }

    protected function extract($payload, ...$names)
    {
        foreach ($names as $name)
            $this->$name = $payload[$name] ?? null;

        return $this;
    }

    protected function defined(array $fields = [])
    {
        $fields = array_values($fields);
        return array_intersect(
            $fields, array_keys(get_class_vars(get_class($this)))
        );
    }
}