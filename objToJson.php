<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 26.02.19
 * Time: 19:24
 */

namespace loandbeholdru\shorts;


class objToJson
{

    public static function throwMethod(&$payload)
    {
        foreach (['toArray', 'export', 'toarray', 'Export'] as $method) {
            $test1 = is_object($payload) && method_exists($payload, $method);
            $payload = $test1 ? json_encode($payload->$method(), JSON_THROW_ON_ERROR) : $payload;
        }


        return $test1;
    }

    public static function throwJson(&$payload)
    {
        $test1 = is_object($payload);
        $payload = $test1 ? json_encode($payload, JSON_THROW_ON_ERROR) : $payload;
        return $test1;
    }

    public static function throwException(&$payload)
    {
        $test1 = $payload instanceof \Throwable;
        $payload = $test1 ? json_encode([ 
            'message' => $payload->getMessage(),
            'code' => $payload->getCode()
        ]): $payload;
        return $test1;
    }

    public static function done(&$payload)
    {
        $test1 = self::throwMethod($payload);
        $test1 = $test1 ? $test1 : self::throwException($payload);
        $test1 = $test1 ? $test1 : self::throwJson($payload);
        
        return $test1;
    }


}