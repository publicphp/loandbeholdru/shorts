<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 06.04.18
 * Time: 10:41
 */

namespace loandbeholdru\shorts;


/**
 * Розвращает регулярное выражение,
 * которое позволяет определить находится ли
 * числовое выражение в диапазоне от 0 до $num
 *
 * Class digitRangeRegExp
 * @package loandbeholdru\shorts
 */
class digitRangeRegExp
{
    protected $num;
    protected $before;
    protected $after;
    public function __construct(int $num, string $before = '\s*', string $after = '\s*')
    {
        $this->num = $num;
        $this->before = $before;
        $this->after = $after;
    }

    public function __toString()
    {
        $pattern = implode('|', $this());
        return "/^$this->before($pattern)$this->after$/";
    }

    public function __invoke()
    {
        $pat = "[0-%d]";
        $cn = "{1,%d}";
        $num = $this->num;
        $digits = str_split((string)$num);
        $dcount = count($digits);
        $res[] = sprintf($pat, 9) . sprintf($cn, $dcount -1);
        $prev = '';
        foreach ($digits as $i => $digit) {
            $line = empty($digit) ? $full = (int)implode($digits) :
                sprintf($pat, $digit - 1) .
                ($i == $dcount -1 ? '' :
                    sprintf($pat, 9) . sprintf($cn, $dcount -1 - $i));
            $res[] = empty($digit) ? $line : $prev . $line;
            $prev .= sprintf($pat, (int)$digit);
        }
        $res[] = empty($full) ? $num : null;
        return array_filter($res);
    }
}