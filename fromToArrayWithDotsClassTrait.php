<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 06.06.18
 * Time: 22:46
 */

namespace loandbeholdru\shorts;


trait fromToArrayWithDotsClassTrait
{
    public function toArray(array $filter = [])
    {
        $_type = static::class;
        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);
        $fields = array_diff($fields, $filter);
        foreach ($fields as $field)
            $result[$field] = $this->$field ?? $this->$field() ??
                $this->payload[$field] ?? static::ifDefined('EMPTY');

        $const = static::ifDefined('PAYLOAD', 'unknownfield');
        $payload = $result[$const] ?? [];
        foreach ($payload as $name => $val)
            if (is_object($val))
                $result[$const][$name] = method_exists($val, 'toArray') ?
                    $val->toArray() : $val;

        return compact(arrays::TYPE_KEY) + arrays::deeparray($result ?? []);
    }

    public static function fromArray(array $data)
    {

        $fields = static::ifDefined('PACK') ??
            static::ifDefined('OPTIONS') ??
            static::ifDefined('OPTS', []);
        foreach ($fields as $field)
            $opts[] = $data[$field] ?? static::ifDefined('EMPTY');

        $type = $data[arrays::TYPE_KEY] ?? static::class;

        $exception = self::ifDefined('EXCEPTION', \Exception::class);

        if (empty($opts) && empty($type))
            throw new $exception(
                '"_type" not found in $data-array and "self::PACK" - not found!'
            );
        foreach ($opts as &$opt)
            $opt = arrays::fromClassified($opt, $opt);

        $last = array_pop($opts);
        $last = is_array($last) ? $last : [$last];
        return (empty($type) || $type === static::class) ?
            new static(...array_merge($opts, $last)) : $type::fromArray($data);
    }
    public function isEmpty()
    {
        return empty(array_filter($this->toArray()));
    }

    public static function ifDefined(string $constname, $default = null)
    {
        return defined(static::class . '::' . $constname) ?
            constant(static::class . '::' . $constname) :
            arrays::fail($constname, $default);
    }
}