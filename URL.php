<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 13.05.18
 * Time: 13:21
 */

namespace loandbeholdru\shorts;


class URL
{
    const PACK = ['scheme', 'domain', 'zone', 'port', 'uri'];
    const REGEXP = "/^\s*(?P<scheme>https?):\/\/" .
                    "(?P<domain>[\.а-яёa-z0-9\-_]{1,60})\." .
                    "(?P<zone>[а-яёa-z0-9]{1,8})" .
                    "(:(?P<port>\d{1,6}))?".
                    "(\/(?P<uri>\S*))?\s*$/";
    const DEFAULT = "/^\s*((?<scheme>https?):\/\/)?" .
                    "(?<domain>[\.а-яёa-z0-9\-_]{1,60})" .
                    "(?<point>\.)?" .
                    "(?<zone>[а-яёa-z0-9]{1,8})?" .
                    "(:(?<port>\d{1,6}))?".
                    "(\/(?<uri>\S*))?\s*$/";
    const URL = [
        'scheme' => '%s://',
        'domain' => '%s',
        'point' => '%s',
        'zone'  => '%s',
        'port'  => ':%s',
        'uri'   => '/%s'
    ];

    public static function scheme(string $url, $default = null)
    {
        return self::toArray($url)['scheme'] ?? arrays::fail($url, $default);
    }
    public static function domain(string $url, $default = null)
    {
        return self::toArray($url)['domain'] ?? arrays::fail($url, $default);
    }
    public static function zone(string $url, $default = null)
    {
        return self::toArray($url)['zone'] ?? arrays::fail($url, $default);
    }
    public static function port(string $url, $default = null)
    {
        return self::toArray($url)['port'] ?? arrays::fail($url, $default);
    }
    public static function uri(string $url, $default = null)
    {
        return self::toArray($url)['uri'] ?? arrays::fail($url, $default);
    }
    public static function fqdn(string $url, $default = null)
    {
        return self::domain($url, $default) . "." . self::zone($url, $default);
    }
    public static function fqdnWithScheme(string $url, $default = null)
    {
        return self::scheme($url, $default) . "://" . self::fqdn($url, $default);
    }
    public static function toArray(string $url, string $template = self::REGEXP)
    {
        preg_match($template, strtolower($url), $fullurl);
        $base = array_combine(self::PACK, array_fill(0,count(self::PACK), ''));
        return array_filter(array_intersect_key($fullurl, $base) + $base);
    }

    public static function default( string $url,
        string $scheme = null, string $domain = null, string $zone = null, string $port = null, string $uri = null
    )
    {
        $complete = self::toArray($url, static::DEFAULT) +
            array_filter(compact('scheme', 'domain', 'zone', 'port', 'uri'));

        $ready = array_intersect_key(static::URL, $complete);
        foreach ($ready as $key => $val)
            $res = ($res ?? '') . sprintf($val, $complete[$key]);
        return $res;
    }

}