<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 26.10.18
 * Time: 15:15
 */

namespace loandbeholdru\shorts;


class timeSpent
{
    protected $start;
    protected $end;

    /**
     * timeSpent constructor.
     * @param $start
     */
    public function __construct()
    {
        $this->start = new \DateTime();
    }


    public static function start()
    {
        return new static();
    }
    //'%I:%S.%f'
    public function __invoke(string $format = null, \DateTime $nextStart = null, \DateTime $now = null)
    {
        $this->end = $now ?? $this->end ?? new \DateTime();
        $result = empty($format) ? ['start' => $this->start, 'end' => $this->end] :
            $this->end->diff($this->start)->format($format);
        $this->start = $nextStart ?? $this->start;
        $result = $result == '59:59' ? '00:01' : $result;
        return $result;
    }

    public function __toString()
    {
        return $this('%I:%S');
    }

}