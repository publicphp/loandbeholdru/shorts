<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 14.08.18
 * Time: 9:40
 */

namespace loandbeholdru\shorts;


/**
 * Доюавляет конструктор, который наполняет свойства объекта данными, переданными в конструктор
 * По списку указанному в константе PACK.
 *
 * Если в константе DIFFERENT находит имя этого свойства, указывающее на класс, то записывает
 * в свойство данные ввиде созданного класса
 *
 * Trait withVarControlConstructor
 * @package loandbeholdru\shorts
 */
trait withVarControlConstructor
{
    protected function __construct(...$args)
    {
        if (!empty($args))
            foreach (static::PACK as $num => $item){
                $this->$item = $args[$num] ?? null;
                $class = static::ifDefined('DIFFERENT', null);
                $class = is_array($class) ? ($class[$item] ?? null) : $class ;
                $range = is_array($class) ? $class : null;
                $class = empty($range) ? $class : null;
                if (!is_null($class))
                    if (!($this->$item instanceof $class))
                        try {
                            $this->$item = $class::fromArray($this->$item);
                        }catch(\Throwable $e){
                            $this->$item = arrays::fromClassified($this->$item);
                        }
                        
                if (!is_null($range))
                    if (!(in_array($this->$item, $range)))
                        throw new \Exception(
                            "$item has to be range member of [" .
                            implode(',', $range) . "]. But give: $this->$item!"
                        );
            }

    }
}