<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 15.06.18
 * Time: 11:20
 */

namespace loandbeholdru\shorts;


class formatter
{
    const FIELD = '/${%s}/';
    
    public static function timeInterval($microtimeinterval, string $formatinterval = '%H:%I:%S.%F')
    {
        $now = new \DateTime();
        $past = new \DateTime();
        $past->setTimestamp(microtime(true) - $microtimeinterval);
        $interval = $past->diff($now);
        return $interval->format($formatinterval);
    }

    public static function byDictionary(string $message, array $dictionary)
    {
        if (!arrays::isAssoc($dictionary)) return $message;
        foreach ($dictionary as $key => $val) {
            try{
                if (is_object($val) || is_array($val))
                    continue;
                settype($val, "string");
            }catch (\Throwable $e){
                continue;
            }

            $field = preg_quote(sprintf(static::FIELD, $key));
            $dict[$field] = $val;
        }
        return preg_replace(array_keys($dict), array_values($dict), $message);
    }
}