<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 26.10.18
 * Time: 15:54
 */

namespace loandbeholdru\shorts;


class progressString
{
    protected $base;
    protected $last;
    protected $count;
    protected $char;
    protected $step;
    protected $points;

    protected $progress = [];
    protected $methods = [ 'decrease','increase', ];

    /**
     * progressString constructor.
     * @param $base
     */
    public function __construct(string $base, int $count = 5, int $step = 1, string $char = '.')
    {
        $this->base = $base;
        $this->count = $count - 1;
        $this->char = $char;
        $this->step = $step;
    }

    public function __invoke(string $add)
    {
        $line = $this->base . $add;

        $last = $this->last - strlen($line) > 0 ?
            str_repeat(' ', $this->last - strlen($line)) : "";

        $this->last = strlen($line);

        return "\r$line$last";
    }

    public function __toString()
    {

        $add = $this->dir();
        $caher = $add();
        return $this($caher);
    }

    protected function dir()
    {
        $test1 = empty($this->progress) || (count($this->progress) > $this->count);
        $this->methods = $test1 ? array_reverse($this->methods) : $this->methods;
        $this->points = $test1 ? $this->step : $this->points;
        return [$this, $this->next()];
    }

    protected function next()
    {
        $test1 = $this->points == $this->step;
        $this->points = $test1 ? 0 : $this->points + 1;
        return $test1 ? end($this->methods) : 'stay';
    }
    public function increase()
    {
        $this->progress[] = $this->char;
        return implode($this->progress);
    }

    public function decrease()
    {
        array_pop($this->progress);
        return implode($this->progress);
    }

    public function stay()
    {
        return implode($this->progress);
    }


}