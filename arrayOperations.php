<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 21.07.17
 * Time: 16:49
 */

namespace loandbeholdru\shorts;


trait arrayOperations
{
    public function getValByPath(string $path,$data){
        $path = str_replace("..","||||" , $path);
        $path = explode('.',trim($path,'.') );
        $ar = $data;
        foreach ($path as &$step) {
            $step = str_replace("||||",".." , $step);
            if (!isset($ar[$step])) return null;
            $ar = $ar[$step];
        }
        return $ar;
    }
    public function buildArrayByPath(string $path, $data = null){
        $path = array_reverse(explode('.',$path ));
        $ret = [];
        $ar = $data;
        foreach ($path as $step) {
            $ret[$step] = $ar;
            $ar = $ret;
            $ret = [];
        }
        return $ar;
    }
    public function any2array($data, $arrayandstring = false){
        if (is_object($data))
            if ( method_exists($data,'toarray') || method_exists($data,'toArray') ){
                $data = $data->toarray();
            } elseif (method_exists($data, 'json') || method_exists($data, 'JSON')){
                $data = json_decode($data->json(),true);
            } elseif (method_exists($data, 'tojson') || method_exists($data, 'toJSON')){
                $data = json_decode($data->tojson(),true);
            }elseif ($data instanceof \stdClass) {
                $data = (array)$data;
            } else {
                $data = json_decode(json_encode($data),true);
            }
        elseif (is_array($data))
            $data = $data;
        else
            $data = [($arrayandstring ? (string)$data : $data)];

        return $data;
    }
    /**
     * Функция меняет значения элементов массива $key и $key2 местами
     * @param array $array исходный массив
     * @param $key ключ элемента массива
     * @param $key2 ключ элемента массива
     * @return bool true замена произошла, false замена не произошла
     */
    public function array_swap(array &$array, $key, $key2)
    {
        if (isset($array[$key]) && isset($array[$key2])) {
            list($array[$key], $array[$key2]) = array($array[$key2], $array[$key]);
            return true;
        }
        return false;
    }

    /** Возвращает true, если заполненных элементов в одномерном массиве >= count
     * Заполненными элементами являются все, которые не равны null или $char
     * @param array $data   - одномерный массив
     * @param int $count    - нижний предел заполненности
     * @param string $char  - символ, который не должен встречаться в элементе массива
     * @return bool
     */
    public function count_valid(array $data, $count = 2, $char = ''){
        $filled = 0;
        foreach ($data as $v) {
            if (is_object($v) || is_array($v)){
                $v = $this->any2array($v);
                $v = implode('',$v );
            }
            if ((!is_null($v)) && ($v !== $char))
                $filled++;
        }
        return $count <= $filled;
    }

    public function implode_ext(string $glue, array $work, string $str = ''){
        if (1 > count($work)) return $str;
        $keys = array_keys($work);
        $key = array_shift($keys);
        //TODO: может быть сделать так, чтобы анализировать содержимое массива и тоже превращать в строку
        $line = [$str,$key,$work[$key]];
        if (1 > strlen($str)) array_shift($line);
        $str = implode($glue, $line);
        unset($work[$key]);
        return $this->implode_ext($glue, $work, $str);
    }

}