<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 15.08.17
 * Time: 0:27
 */

namespace loandbeholdru\shorts;


/**
 * Объединяет функции комбинационных операций с массивами
 * Class combineOperations
 * @package App\Utils
 */
trait combineOperations
{
    use arrayOperations;

    /**
     * Функция возвращает все полные комбинации одномерного массива.
     * Так для массива ['1','2','3','4'] функция вернет:
     * [['1','2','3','4'], ['1','2','4','3'], ['1','4','2','3'] ... ['2','1','3','4']]
     * @param array $work - одномерный массив
     * @return array
     */
    public function mainCombines(array $work){
        $ret = [];
        $status = array_keys($work);
        foreach ($status as $num)
            foreach ($work as $i => $v)
                if ($this->array_swap($work, $i + 1, $i))
                    array_push($ret, $work);
        return array_values($ret);
    }

    /**
     * Для двумерного массива поных комбинаций, функция
     * возвращает двумерный массив неполных комбинацй.
     * Так для массива [['1','2','3','4'], ['1','2','4','3'], ['1','4','2','3'] ... ['2','1','3','4']]
     * функция вернет:
     * [['1','2','3'], ['1','2','4'], ['1','4'] ... ['2']]
     * @param array $work - двумерный массив неполных комбинацй.
     * @param int $limit - ограничение минимального количества членов комбинации
     * @return array
     */
    public function poorCombines(array $work, int $limit = 1){
        $et = $work;
        foreach ($work as $key=>$val) {
            if (!is_array($val)) continue;
            if (count($val) > $limit){
                array_pop($val);
                $work[$key] = $val;
            }
        }
        if ($et === $work) return [];
        $work = array_unique($work, SORT_REGULAR);
        return array_merge($work, $this->poorCombines($work,$limit));
    }

    /**
     * Функция возвращает все комбинации (и полные и неполные)
     * для одномерного массива.
     * Так для массива ['1','2','3','4'] функция вернет:
     * [['1','2','3','4'], ['1','2','4','3'], ['1','4','2','3'] ... ['2','1','3','4'],
     *  ['1','2','3'], ['1','2','4'], ['1','4'] ... ['2']]
     * @param array $work - одномерный массив.
     * @param int $limit - ограничение минимального количества членов комбинации.
     * @param array $work
     * @param int $limit
     * @return array
     */
    public function allCombines(array $work, int $limit = 2){
        $work = array_values($work);
        $main = $this->mainCombines($work);
        $poor = $this->poorCombines($main,$limit);
        return array_merge($poor,$main);
    }

    /**
     * Функция возвращает все варианты сочетаний массивва в виде массива строк.
     * Так для массива: ['mail' => ['m@ya.ru','t@ya.ru'], 'name' => ['1','2']]
     * Функция вернет:
     * [ 'mail m@ya.ru name 1','mail m@ya.ru name 2', 
     *      'mail t@ya.ru name 1', 'mail t@ya.ru name 2']
     * @param array $work - двумерный массив
     * @param bool $withkeys - вставлять ли ключи в строку
     * @param string $str - строка, с которой начинать 
     * @return array 
     */
    public function allVariants(array $work, bool $withkeys = true, string $str = ''){
        if (count($work) < 1 ) return [$str];
        $res = [];
        foreach ($work as $key => $value) {
            if (is_array($value)){
                if (count($value) === 0){
                    $str = $withkeys ? $this->implode_ext(' ', [$key => ''], $str) : $str;
                    unset($work[$key]);
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        if (count($work) < 1 ) return [$str];
        $fields = array_keys($work);
        $field = array_shift($fields);
        
        $data = is_array($work[$field]) ? $work[$field] : [$work[$field]];
        unset($work[$field]);
        foreach ($data as $key => $value) {
            $send = $withkeys ? $this->implode_ext(' ',[$field => $value], $str) : "$str $value";
            $res = array_merge($res, $this->allVariants($work, $withkeys, $send));
        }
        return $res;
    }

    /**Функция возвращает массив, состоящих из всех вариантов заполнения полей массива.
     * Все "неполные" заполнения, с ограничением в $limit
     * @param array $work   - массив
     * @param int $limit    - ограничение
     * @param string $char  - символ пустоты поля (пустое поле содержит либо null либо этот символ)
     * @return array
     */
    public function incomplete(array $work, int $limit = 2, $char = ''){
        ksort($work);
        $keys = array_keys($work);
        $clean = array_combine($keys, array_fill(0, count($keys), $char));
        $list = $this->poorCombines($this->mainCombines($keys),$limit);
        foreach ($list as $key => $line) {
            sort($line);
            $list[$key] = $line;
        }
        $list = array_values(array_unique($list,SORT_REGULAR));
        $ret = [];
        foreach ($list as $key => $line) {
            $new = $clean;
            foreach ($line as $field)
                $new[$field] = $work[$field];
            if ($this->count_valid($new,$limit,$char))
                $ret[] = $new;
        }
        return array_values(array_unique($ret,SORT_REGULAR));
    }


}