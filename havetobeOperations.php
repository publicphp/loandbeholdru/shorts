<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 15.08.17
 * Time: 23:22
 */

namespace loandbeholdru\shorts;


trait havetobeOperations
{

    /**
     * Убирает символы из чарлиста по кряаям строки при помощи preg_replace
     * @param $string - строка
     * @param null $charlist - чарлист
     * @return mixed|string
     */
    public function mb_trim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return trim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/(^[$charlist]+)|([$charlist]+$)/us", '', $string);
        }
    }

    /**
     * Убирает символы из чарлиста по правому краю строки при помощи preg_replace
     * @param $string - строка
     * @param null $charlist - чарлист
     * @return mixed|string
     */
    public function mb_rtrim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return rtrim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/([$charlist]+$)/us", '', $string);
        }
    }

    /**
     * Убирает символы из чарлиста по левому краю строки при помощи preg_replace
     * @param $string - строка
     * @param null $charlist - чарлист
     * @return mixed|string
     */
    public function mb_ltrim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return ltrim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/(^[$charlist]+)/us", '', $string);
        }
    }

    /**
     * Проверяет весь ли список из dict есть в виде ключей в массиве work.
     * Если work не ассоциативный массив, проверяет на наличие dict в массиве (как in_array)
     * Если dict ассоциативный массив, то берет его ключи
     * @param array $work
     * @param array $dict
     * @return bool
     */
    public function key_exist_mass(array $work, array $dict)
    {
        $work = $this->is_assoc($work) ? array_keys($work) : $work;
        $dict = $this->is_assoc($dict) ? array_keys($dict) : $dict;
        foreach ($dict as $value) {
            if (!in_array($value, $work))
                return false;
        }
        return true;
    }

    /**
     * Проверяет ассоциативный ли массив или нет
     * @param array $array
     * @return bool
     */
    public function is_assoc($array)
    {
// Keys of the array
        if (!is_array($array)) return false;

        $keys = array_keys($array);

// If the array keys of the keys match the keys, then the array must
// not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }

    public function ready2Extract(array $template, array $source)
    {
        foreach ($template as $key => $value)
            $template[$key] = isset($source[$value]) ? $source[$value] : null;
        return $template;

    }

    /**Функция оставляет в массиве те значения, что указаны в фильтре
     *
     * @param array $work - исходный массив
     * @param $filter - фильтр (строка, массив, ассоциативный массив)
     * @param string|null $charlist - перечень символов,
     *          которые должны быть обрезаны по краям ключей фильтра (работает для строки)
     * @return array
     */
    public function pickArrayByKeys(array $work, $filter, string $charlist = null)
    {
        $filter = $this->buildAssocFilter($filter, $charlist);
        return array_intersect_key($work, $filter);
    }

    /**Функция оставляет в массиве те значения, что не указаны в фильтре
     *
     * @param array $work - исходный массив
     * @param $filter - фильтр (строка, массив, ассоциативный массив)
     * @param string|null $charlist - перечень символов,
     *          которые должны быть обрезаны по краям ключей фильтра (работает для строки)
     */
    public function filterArrayByKeys(array $work, $filter, string $charlist = null)
    {
        $filter = $this->buildAssocFilter($filter, $charlist);
        return array_diff_key($work, $filter);
    }

    /**
     * Функция преобразовывает строку или массив в ассоциативный массив пустых значений
     * Разделитель - пробел. Обрезаемые по краям символы - $charlist
     * @param $filter - строка или массив
     * @param string|null $charlist - перечень символов, которые нужно обрезать по краям ключей
     * @return mixed
     */
    public function buildAssocFilter($filter, string $charlist = null)
    {
        if (is_string($filter)) {
            $filter = explode(' ', $filter);
            foreach ($filter as $i => $value) {
                if ($value === '')
                    unset($filter[$i]);
                else
                    $filter[$i] = $this->mb_trim($value, $charlist);
            }
            $filter = array_values($filter);
        }
        if (!is_array($filter)) return [];
        return $this->is_assoc($filter) ? $filter : array_fill_keys($filter, "");
    }

    /** Функция возвращает массив с подставленными данными в строку формата
     * @param string $format
     * @param array $data
     * @return array
     */
    public function vsprintf_mass(string $format, array $data) : array
    {
        $ret = [];
        foreach ($data as $line) {
            $line = is_array($line) ? $line : [$line];
            $ret[] = vsprintf($format, $line);
        }
        return $ret;
    }

    /**
     * Функция разбирает строку в ассоциативный массив
     * Обязательное условие - поля и данные разделены пробеламим.
     * За именем поля следует его содержимое.
     * Список полей принимает в виде строки либо массива (ассоциативный или нет - безразлично)
     * @param string $str - строка с данными
     * @param array|string $keys - список полей
     * @param string $trimlist - лист с символами для усушки найденного 
     */
    public function string2Assoc(string $str, $keys, string $trimlist = null)
    {
        if ((!is_string($keys)) && (!is_array($keys))) return [];
        $keys = array_keys($this->buildAssocFilter($keys, $trimlist));
        $template = '(%1$s\s+)';
        $list = $this->vsprintf_mass($template, $keys);
        $pattern = "/" . implode('|', $list) . "/";
        $res = preg_split($pattern, $str,-1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        if (!is_array($res)) return [];
        $k = $v = [];
        foreach ($res as $val) {
            $val = $this->mb_trim($val, $trimlist);
            if (in_array($val, $keys))
                $k[] = $val;
            elseif ($val === "")
                continue;
            else
                $v[] = $val;
        }
        return array_combine($k, $v);
    }

    /**
     * Функция делает ассоциативный массив из двух строк/массивов/ассоциативных массивов
     * В случае строк, разделителями между полями является пробел.
     * Если количество ключей и данных не совпадает, то недостающие данные заменяются ""
     * Лишние данные - обрезаются
     * @param $keys
     * @param $values
     * @param string|null $trimlist
     * @return mixed
     */
    public function array_combine_ext($keys, $values , string $trimlist = null){
        $keys = $this->buildAssocFilter($keys,$trimlist);
        $vals = array_keys($this->buildAssocFilter($values));
        $i = 0;
        foreach ($keys as $k => $v) {
            if (array_key_exists($i, $vals))
                $keys[$k] = $vals[$i];
            $i++;
        }
        return $keys;
    }

    /**
     * Функция получает на входе массив строк. Слова через пробел.
     * На выходе, нарезанные на столбики текст (массив 1-х слов, массив 2-х слов, ...). Заголовки берет из $header
     * @param array $work
     * @param null $header
     * @param string $placeholder
     * @return array
     */
    public function streamSplit(array $work, $header = null, string $placeholder = ''){
        $parts = [];
        foreach ($work as $n => $str) {
            $line = preg_split('/(\s+)/', $str );
            if (!$line) continue;
            foreach ($line as $i => $v) {
                if (!isset($parts[$i])) $parts[$i] = [];
                $parts[$i][$n] = $v;
            }
        }
        $c = count($work);
        for ($n = 0; $n < $c; $n++){
            foreach ($parts as $i => $v)
                if (!isset($parts[$i][$n]))
                    $parts[$i][$n] = $placeholder;
            ksort($parts[$i]);
        }

        if (is_null($header)) return $parts;
        $header = $this->buildAssocFilter($header);
        $header = $this->array_combine_ext(array_keys($parts), array_keys($header));
        return array_combine($header, $parts);
        
    }

    /**
     * Функция работает как стандартный array_flip, 
     * но заменяет все дуплицированные ключи нумерованным плейсхолдером
     * @param array $work - транспанируемый массив
     * @param string $placeholder - строка плейсхолдера
     * @return array
     */
    public function array_flip_ext(array $work, string $placeholder = 'noname'){
        $ret = [];
        $i = 0;
        foreach ($work as $key => $value) {
            if (array_key_exists($value, $ret ) || is_null($value)) 
                $value = $placeholder . $i++;
            $ret[$value] = $key;
        }
        return $ret;
    }
}