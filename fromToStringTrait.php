<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 24.05.18
 * Time: 19:32
 */

namespace loandbeholdru\shorts;


trait fromToStringTrait
{
    static $serializer = 'json_encode';
    static $unserializer = 'json_decode';

    protected function identity()
    {
        self::$serializer = 'json_encode';
        self::$unserializer = 'json_decode';
    }
    public function __toString()
    {
        return (self::$serializer)($this->toArray());
    }

    public static function fromString($json)
    {
        $result = (self::$unserializer)($json);
        $result = is_array($result) ? $result : (array)$result;
        $result = arrays::deeparray($result);
        return self::fromArray($result);
    }

    public function equal(self $payload)
    {
        return arrays::isEqual($this->toArray(), $payload->toArray());
    }

    public function same(self $payload, array $filtred = [])
    {
        $filter = array_flip($filtred);
        return arrays::isEqual(
            array_diff_key($this->toArray(), $filter),
            array_diff_key($payload->toArray(), $filter)
        );
    }

}