<?php
/**
 * Created by PhpStorm.
 * User: mopkob
 * Date: 08.06.18
 * Time: 16:17
 */

namespace loandbeholdru\shorts;


/**
 * Задаешь соответствие имени класса и тмени переменной в массиве $this->map и
 * можешь в любоим порядке настроить свой объект.
 *
 * Trait mapSetupTrait
 * @package loandbeholdru\shorts
 */
trait mapSetupTrait
{
    public function setup(...$options)
    {
        foreach ($options as $option) {
            $classes = array_merge(
                [get_class($option)], class_parents($option), class_implements($option)
            );
            foreach ($classes as $class) {
                $field = $this->map[$class] ?? null;
                if (empty($field)) continue;
                $this->$field = $option;
            }
        }
        return $this;
    }

    public function reinit(...$options)
    {
        $this->setup(...$options);
        if (method_exists($this, 'init'))
            $this->init();
        return $this;
    }
}