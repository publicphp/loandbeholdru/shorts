# shorts

Use this tool for simple operation with strings and arrays.

## Installation

It's recommended that you use [Composer](https://getcomposer.org/) to install shorts.

```bash
$ composer require loandbeholdru/shorts
```

## TODO / in progress

Package include many functions which i use more than 5 years, but this README so poor.  
Documentation in progress ... 

## Usage

Replaces the fields in the $payload text according to the $dict dictionary. ReGexp field template takes from $field regexp
$dict is an associative array. Field names in $keys, field contents in $values

```
arrays::substfields(string $payload, array $dict, string $fieldregexp = "/#\{%s\}/")
```

Switch keys of assoc array to low reg:    
```php
arrays::lowkeys(array $payload)
```

Changes $payload to array. If $payload is array do it recursive:  
```php
arrays::always($payload)
```

Deep sorting and comparing two arrays:  
```php
arrays::isEqual(array $ar1, array $ar2)
```

Returns true if all array elements belong to the $type type:  
```php
arrays::is_array_of($payload, $sample, bool $anywayarray = false)
```

Combines an array of two different ones. Regardless of the length. Finishes off all $ by default:
```php
arrays::args($names, $values, $default = null)
```

Recursively sorts the array by keys or by content (if it is ordinary):  
```php
arrays::sort(&$payload)
```

Defines the associativity of an array:  
```php
arrays::isAssoc($payload)
```

Returns an instance of a class made from an array,
if the array contains information about the class in the field with the name:
arrays::TYPE_KEY ("_type")  
```php
arrays::fromClassified($classified, $default = null)
```

Handles "fail" situations. If $default is a function, executes $default($data).
If $default is an exception, it throws an exception.
In other cases, it returns $default:  
```php
arrays::fail($data, $default)
```

Returns decoded JSON, acting in the same way as json_decode,
only determines the validity of json. In case of invalid JSON returns
either default or the result of default execution (or Exception):    
```php
arrays::valid_json($string, bool $assoc = true, $default = null)
```

First, drive everything to an array (over the entire depth),
then to json, and then takes a hash:    
```php
arrays::deephash($payload, string $algo = 'ripemd128')
```

Last element:  
```php
arrays::last(array $payload)
```

First element:  
```php
arrays::first(array $payload)
```

Checks for the presence of a constant in the class or globally. $default responds:  
```php
arrays::ifDefined(string $classname, string $constname, $default = null)
```

## Contributions

... are always welcome. Many times it is useful to just point out a use case the author have not thought about or come across.


